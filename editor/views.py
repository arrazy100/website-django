from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import F, Q
from django.http import (HttpResponse, HttpResponsePermanentRedirect,
						 HttpResponseRedirect)
from django.shortcuts import get_object_or_404, redirect, render
from django.template.defaultfilters import slugify
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from artikel.forms import *
from artikel.models import *


# Create your views here.
class e_artikel(LoginRequiredMixin, ListView):
	model = artikel
	form_class = form_pencarian
	context_object_name = "post"
	template_name = "editor_artikel.html"
	paginate_by = 4

	# login required
	login_url = "/login/"
	redirect_field_name = "redirected_to"

	def get_context_data(self, **kwargs):
		context = super(e_artikel, self).get_context_data(**kwargs)
		context["form"] = form_pencarian()
		return context

	def get_queryset(self):
		# search form handling
		form = self.form_class(self.request.GET)
		queryset = artikel.objects.all()
		if form.is_valid():
			queryset = artikel.objects.filter (Q(judul__icontains = form.cleaned_data["cari"]) | Q(kategori__icontains = form.cleaned_data["cari"]) | Q(tag__icontains = form.cleaned_data["cari"]))

		return queryset

class v_buat_artikel(LoginRequiredMixin, CreateView):
	# login required
	login_url = "/login/"
	redirect_field_name = "redirected_to"

	template_name = "buat_artikel.html"
	form_class = form_artikel
	success_url = reverse_lazy("artikel")

class v_edit_artikel(LoginRequiredMixin, UpdateView):
	# login required
	login_url = "/login/"
	redirect_field_name = "redirected_to"

	template_name = "edit_artikel.html"
	model = artikel
	form_class = form_artikel
	success_url = reverse_lazy("artikel")

	def get_initial(self):
		# get initial kategori from database
		if self.object.kategori:
			data = list_kategori.objects.get(nama = self.object.kategori)
			return {"kategori": data.id}

def v_hapus_artikel(self, slug):
	post = get_object_or_404(artikel, slug = slug)
	post.delete()

	return redirect("artikel")
