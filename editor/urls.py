from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static

from editor import views

urlpatterns = [
	url(r'^editor/$', views.e_artikel.as_view(), name='editor_artikel'),
	url(r'^editor/baru', views.v_buat_artikel.as_view(), name='buat_artikel'),
	url(r'^editor/(?P<slug>[\w-]+)/edit', views.v_edit_artikel.as_view(), name='edit_artikel'),
	url(r'^editor/(?P<slug>[\w-]+)/hapus', views.v_hapus_artikel, name='hapus_artikel'),
]
