# Generated by Django 2.0.2 on 2019-05-15 04:37

import artikel.storage
import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('artikel', '0006_auto_20190515_0930'),
    ]

    operations = [
        migrations.AlterField(
            model_name='artikel',
            name='cover',
            field=models.ImageField(storage=artikel.storage.OverwriteStorage(), upload_to='cover_artikel'),
        ),
        migrations.AlterField(
            model_name='artikel',
            name='tanggal',
            field=models.DateField(default=datetime.datetime(2019, 5, 15, 11, 37, 26, 994839)),
        ),
        migrations.AlterField(
            model_name='komentar',
            name='tanggal',
            field=models.DateField(default=datetime.datetime(2019, 5, 15, 11, 37, 26, 994839)),
        ),
    ]
