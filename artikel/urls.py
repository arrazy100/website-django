from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static

from artikel import views

urlpatterns = [
	url(r'^$', views.v_artikel.as_view(), name='artikel'),
	url(r'^artikel/(?P<slug>[\w-]+)', views.v_detail_artikel, name='detail_artikel'),
]

# cover image urls
if settings.DEBUG:
	urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
