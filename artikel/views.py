import datetime
import os

from django.conf import settings
from django.db.models import F, Q
from django.http import (HttpResponse, HttpResponsePermanentRedirect,
                         HttpResponseRedirect)
from django.shortcuts import get_object_or_404, redirect, render
from django.template.defaultfilters import slugify
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from .forms import *
from .models import *


# Create your views here.
class v_artikel(ListView):
	model = artikel
	form_class = form_pencarian
	context_object_name = "post"
	template_name = "artikel.html"
	paginate_by = 4

	def get_context_data(self, **kwargs):
		context = super(v_artikel, self).get_context_data(**kwargs)
		context["form"] = form_pencarian()
		# send top 5 popular posts to context
		context["populer"] = artikel.objects.all().order_by("-counter")[:5]
		return context

	def get_queryset(self):
		form = self.form_class(self.request.GET)
		queryset = artikel.objects.all()
		if form.is_valid():
			# search articles from user input that corresponds judul, kategori, or tag
			queryset = artikel.objects.filter (Q(judul__icontains = form.cleaned_data["cari"]) | Q(kategori__icontains = form.cleaned_data["cari"]) | Q(tag__icontains = form.cleaned_data["cari"]))

		return queryset

def v_detail_artikel(request, slug):
	template = "detail_artikel.html"
	post = get_object_or_404(artikel, slug = slug)

	context = {"post": post}

	# add counter visitor
	if not slug in request.session:
		post = artikel.objects.filter(slug = slug).update(counter = F("counter")+1)
		request.session[slug] = slug

	return render(request, template, context)
