import datetime
import os
import re

from ckeditor.fields import RichTextField
from django.core.exceptions import ValidationError
from django.core.validators import validate_image_file_extension
from django.db import models
from django.db.models.signals import (post_delete, post_init, post_save,
                                      pre_save)
from django.dispatch import receiver
from django.template.defaultfilters import slugify

from .storage import OverwriteStorage


def file_size(value):
	# set file size limit to 1 MB
	limit = 1 * 1024 * 1024
	# check if uploaded file size > 1 MB
	if value.size > limit:
		raise ValidationError("File too large. Size limit: 1 MB")

def content_file_name(instance, filename):
	ext = filename.split(".")[-1] # get file extension
	file = filename.split(".")[0] # get file name
	filename = "%s_%s.%s" % (file, instance.id, ext)
	return os.path.join("cover_artikel", filename)

# Create your models here.
class artikel(models.Model):
	class Meta:
		db_table = "artikel"

	id = models.AutoField(primary_key = True)
	judul = models.CharField(max_length = 64)
	slug = models.SlugField(default = "", editable = False, max_length = 100)
	tanggal = models.DateField(default = datetime.datetime.now())
	cover = models.ImageField(storage = OverwriteStorage(), upload_to = content_file_name, validators=[validate_image_file_extension, file_size])
	isi = RichTextField()
	tag = models.CharField(blank = True, null = True, max_length = 64)
	kategori = models.CharField(blank = True, null = True, max_length = 64)
	counter = models.PositiveIntegerField(default = 0)

	def __str__(self):
		return self.judul

	def clean(self):
		a = artikel.objects.filter(judul = self.judul)
		# check if judul id is exist
		try:
			b = a.values_list("id", flat = True)[0]
		except:
			b = -1

		# raise error if article is exist and not original article (while modifying)
		if a.exists() and b != self.id:
			raise ValidationError("Judul artikel sudah ada")

class komentar(models.Model):
	post = models.ForeignKey(artikel, on_delete = models.CASCADE)
	nama = models.CharField(max_length = 64)
	tanggal = models.DateField(default = datetime.datetime.now())
	isi = models.TextField()

	def __str__(self):
		return self.nama + ': ' + self.isi

class list_kategori(models.Model):
	id = models.AutoField(primary_key = True)
	nama = models.CharField(max_length = 64)

	def __str__(self):
		return self.nama

# artikel signal
@receiver(pre_save, sender = artikel)
def convert_data(sender, instance, **kwargs):
	# create slug from judul before create or modifying an article
	instance.slug = slugify(instance.judul)

@receiver(post_init, sender = artikel)
def backup_path(sender, instance, **kwargs):
	# backup old cover image
	instance._current_cover = instance.cover

@receiver(post_save, sender = artikel)
def del_unused_data(sender, instance, **kwargs):
	# delete unused image after modifying an article if old cover image not same as new cover image
	if hasattr(instance, "_current_cover"):
		if instance._current_cover != instance.cover:
			instance._current_cover.delete(False)

@receiver(post_delete, sender = artikel)
def submission_delete(sender, instance, **kwargs):
	# delete cover image on disk after delete an article
	instance.cover.delete(False)
