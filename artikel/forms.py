import re

from ckeditor.widgets import CKEditorWidget
from django import forms
from django.core.exceptions import ValidationError

from .models import *


class form_artikel(forms.ModelForm):
	kategori = forms.ModelChoiceField(required = False, queryset = list_kategori.objects.all(), empty_label="Pilih Kategori")
	class Meta:
		model = artikel
		fields = ["judul", "cover", "isi", "tag", "kategori"]

	def __init__(self, *args, **kwargs):
		super(form_artikel, self).__init__(*args, **kwargs)
		self.fields["tag"].required = False
		self.fields["isi"].widget = CKEditorWidget()

	def clean_tag(self):
		data = self.cleaned_data["tag"]
		# raise error if data contains other than lowercase and ,
		if re.findall("[^ ,a-z]", data):
			raise ValidationError("tags only lowercase, for multiple tags use (,)")
		return data

class form_komentar(forms.ModelForm):
	class Meta:
		model = komentar
		fields = ["nama", "isi"]

class form_kategori(forms.ModelForm):
	nama = forms.CharField(max_length = 64, required = False)
	class Meta:
		model = list_kategori
		fields = ["nama"]

	def clean_nama(self):
		data = self.cleaned_data["nama"]
		# raise error if data contains characters other than alphabetical
		if re.findall("[^ a-zA-Z]", data):
			raise ValidationError("Kategori hanya huruf")
		return data

class form_pencarian(forms.Form):
	cari = forms.CharField(required = False, max_length = 64)
